﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPersonajes : MonoBehaviour
{
    [SerializeField] GameObject Personaje1;
    [SerializeField] GameObject Personaje2;
    [SerializeField] GameObject Personaje3;
    [SerializeField] GameObject Personaje4;
    [SerializeField] GameObject Personaje5;
    [SerializeField] GameObject Personaje6;
    [SerializeField] Transform Marker;
    private GameObject persona;
    private GameObject persona2;
    private GameObject persona3;
    private GameObject persona4;
    private GameObject persona5;
    private GameObject persona6;
    public static int rango;
    Desicion Desicion;
    int chau2;
    public static bool termino;
    void Start()
    {
        Vector3 currentPosition1 = Marker.position;
        currentPosition1.z = 14.7f;
        Marker.position = currentPosition1;

        rango = Random.Range(3, 6);
        for (int i = 0; i <= rango; i++) //
        {
            if (i == 1) 
            {
                persona = Instantiate(Personaje1, Marker.position, Personaje1.transform.rotation);
                Vector3 currentPosition = Marker.position;
                currentPosition.z -= 5f;
                Marker.position = currentPosition;
            }
            if (i == 2)
            {
                persona2 = Instantiate(Personaje2, Marker.position, Personaje2.transform.rotation);
                Vector3 currentPosition = Marker.position;
                currentPosition.z -= 5f;
                Marker.position = currentPosition;
            }
            if (i == 3)
            {
                persona3 = Instantiate(Personaje3, Marker.position, Personaje3.transform.rotation);
                Vector3 currentPosition = Marker.position;
                currentPosition.z -= 5f;
                Marker.position = currentPosition;
            }
            if (i == 4)
            {
                persona4 = Instantiate(Personaje4, Marker.position, Personaje4.transform.rotation);
                Vector3 currentPosition = Marker.position;
                currentPosition.z -= 5f;
                Marker.position = currentPosition;
            }
            if (i == 5)
            {
                persona5 = Instantiate(Personaje5, Marker.position, Personaje5.transform.rotation);
                Vector3 currentPosition = Marker.position;
                currentPosition.z -= 5f;
                Marker.position = currentPosition;
            }
            if (i == 6)
            {
                persona6 = Instantiate(Personaje6, Marker.position, Personaje6.transform.rotation);
                Vector3 currentPosition = Marker.position;
                currentPosition.z -= 5f;
                Marker.position = currentPosition;
            }
        }
        persona.transform.SetParent(Marker); //el ultimo personaje instanciado es puesto como hijo de Personajes
        chau2 = 6 - rango;
        Desicion.chau = chau2;
    }

    void Update()
    {
        termino = false;
        chau2 = Desicion.chau;
        if (persona6 != null)
        {
            if (chau2 == 1)
            {
                Destroy(persona6);
                Debug.Log("Chau");
            }
            Vector3 currentPosition2 = persona6.transform.position;
            currentPosition2.z = -2f;
            persona6.transform.position = currentPosition2; // Si supiera como hacer funcionar las animaciones con codigo rotaria al personaje segun sea aceptado o rechazado con un bool desde Desicion
        }
        else
        {
            if (persona5 != null)
            {
                if (chau2 == 2)
                {
                    Destroy(persona5);
                    Debug.Log("Chau");
                }
                Vector3 currentPosition2 = persona5.transform.position;
                currentPosition2.z = -3f;
                persona5.transform.position = currentPosition2;
            }
            else
            {
                if (persona4 != null)
                {
                    if (chau2 == 3)
                    {
                        Destroy(persona4);
                        Debug.Log("Chau");
                    }
                    Vector3 currentPosition2 = persona4.transform.position;
                    currentPosition2.z = -3f;
                    persona4.transform.position = currentPosition2;
                }
                else
                {
                    if (persona3 != null)
                    {
                        if (chau2 == 4)
                        {
                            Destroy(persona3);
                            Debug.Log("Chau");
                        }
                        Vector3 currentPosition2 = persona3.transform.position;
                        currentPosition2.z = -3f;
                        persona3.transform.position = currentPosition2;
                    }
                    else
                    {
                        if (persona2 != null)
                        {
                            if (chau2 == 5)
                            {
                                Destroy(persona2);
                                Debug.Log("Chau");
                            }
                            Vector3 currentPosition2 = persona2.transform.position;
                            currentPosition2.z = -3f;
                            persona2.transform.position = currentPosition2;
                        }
                        else
                        {
                            if (persona != null)
                            {
                                if (chau2 == 6)
                                {
                                    Destroy(persona);
                                    Debug.Log("Chau");
                                }
                                Vector3 currentPosition2 = persona.transform.position;
                                currentPosition2.z = -3f;
                                persona.transform.position = currentPosition2;
                            }
                        }
                    }
                }
            }
        }
        if (Marker.childCount == 0)
        {
            Desicion.chau = 0;
            termino = true;
            Start();
        }
    }
}
