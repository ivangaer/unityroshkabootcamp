﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Tarjeta : MonoBehaviour
{
    int i = 0;
    private Text Nombre;
    Utils Utils;
    public static float Edad;
    public static bool Siguiente = true;
    ControlPersonajes ControlPersonajes;
    void Start()
    {
        Nombre = GetComponent<Text>();
    }
    void nombrar()
    {
        if (i < ControlPersonajes.rango)
        {
            string playerName = Utils.RandomName();
            Edad = Random.Range(18, 35);
            Nombre.text = playerName + Edad;
            FindObjectOfType<AudioManager>().Play("Sacar");
            i++;
        }
        else
        {
            Nombre.enabled = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Siguiente == true)
        {
            nombrar();
            Siguiente = false;
        }
        if(ControlPersonajes.termino == true)
        {
            i = 0;
            Nombre.enabled = true;
        }
    }
}
