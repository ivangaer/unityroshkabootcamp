﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Nuevo Personaje", menuName = "Persona")]

public class Personaje : ScriptableObject
{
    public GameObject Cabello;
    public GameObject Ropa;
    public GameObject Ropa1;
    public GameObject Ropa2;
    public Material Cara;
    public GameObject Cuerpo;
    public string playerName;
    public int Edades;
}
