﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Desicion : MonoBehaviour // Esta escrito mal a proposito 
{
    private SwerveInputSystem _swerveInputSystem;
    [SerializeField] private float swerveSpeed = 0.5f;
    private bool siguiente = true;
    private float tiempo = 3f;
    private float sigtiempo;
    [SerializeField] Text Correcto;
    [SerializeField] Text Incorrecto;
    private int correctos;
    private int incorrectos;
    Tarjeta Tarjeta;
    ControlPersonajes ControlPersonajes;
    public static int chau = 0;

    int i=0;
    void Awake()
    {
        _swerveInputSystem = GetComponent<SwerveInputSystem>();
    }
    void Start()
    {

    }
    void Update()
    {
        if (i < ControlPersonajes.rango)
        {
            float swerveAmount = Time.deltaTime * swerveSpeed * _swerveInputSystem.MoveFactorX;
            if (swerveAmount > 0.1f && siguiente == true)
            {
                i++;
                Debug.Log("Aceptado");
                //Aceptar
                Tarjeta.Siguiente = true;
                sigtiempo = tiempo;
                siguiente = false;
                FindObjectOfType<AudioManager>().Play("Aceptado");
                if (Tarjeta.Edad >= 21) //Se agregaria otra condicion para remera blanca, etc.
                {
                    Puntaje.PuntajePrin += 15;
                    correctos += 1;
                    Correcto.text = "Correctos :" + correctos;
                    chau += 1;
                }
                if (Tarjeta.Edad < 21) //Se agregaria otra condicion para remera blanca, etc.
                {
                    Puntaje.PuntajePrin -= 20;
                    incorrectos += 1;
                    Incorrecto.text = "Incorrectos :" + incorrectos;
                    chau += 1;

                }
                Debug.Log("Esperar a que el sujeto salga de la pantalla");
                //Si supiera como hacerlo, haria que el bool creado en el animator llamado 'pasa' lo pondria en true a la vez que pondria el Festeja? del animator del personaje en true
            }
            if (swerveAmount < -0.1f && siguiente == true)
            {
                i++;
                Debug.Log("Rechazar");
                //Rechazar
                Tarjeta.Siguiente = true;
                sigtiempo = tiempo;
                siguiente = false;
                FindObjectOfType<AudioManager>().Play("Rechazado");
                if (Tarjeta.Edad < 21) //Se agregaria otra condicion para remera blanca, etc.
                {
                    Puntaje.PuntajePrin += 15;
                    correctos += 1;
                    Correcto.text = "Correctos :" + correctos;
                    chau += 1;
                }
                if (Tarjeta.Edad > 21) //Se agregaria otra condicion para remera blanca, etc.
                {
                    Puntaje.PuntajePrin -= 20;
                    incorrectos += 1;
                    Incorrecto.text = "Incorrectos :" + incorrectos;
                    chau += 1;
                }
                Debug.Log("Esperar a que el sujeto salga de la pantalla");
                //Si supiera como hacerlo, haria que el bool creado en el animator llamado 'No pasa' lo pondria en true a la vez que pondria el No pasa? del animator del personaje en true
            }
        }
        sigtiempo -= Time.deltaTime;
        if (sigtiempo < 0)
        {
            sigtiempo = 0;
            siguiente = true;
        }
        if (ControlPersonajes.termino == true)
        {
            i = 0;
        }
    }
}
