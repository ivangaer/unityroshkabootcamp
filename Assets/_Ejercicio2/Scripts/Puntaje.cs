﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntaje : MonoBehaviour
{
    public RectTransform TextoPuntaje;
    public static float PuntajePrin = 0;
    private Text textpuntaje;
    private float aux = 0;
    void Start()
    {
        textpuntaje = GetComponent<Text>();
    }

    void Update()
    {
        textpuntaje.text = "Puntaje : " + PuntajePrin; //Se actualiza el puntaje
        if (PuntajePrin > aux) //Utiliza LeanTween para hacer que el texto del puntaje haga una animacion 
        {
            LeanTween.scale(TextoPuntaje, TextoPuntaje.localScale * 1.05f, 0.2f).setEase(LeanTweenType.easeInOutQuint).setLoopPingPong(1);
            aux = PuntajePrin;
            TextoPuntaje.localScale = Vector3.Scale(TextoPuntaje.localScale, new Vector3(1f, 1f, 1f)); //Devuelve los valores de tamaño del texto al default
        }
        
    }

}
