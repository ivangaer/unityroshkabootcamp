﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Carpetas : MonoBehaviour
{
    [SerializeField] Text Mostrar1;
    [SerializeField] Button Mostrar2;
    [SerializeField] Image Mostrar3;
    [SerializeField] Text Ocultar1;
    [SerializeField] Button Ocultar2;
    [SerializeField] Image Ocultar3;
    [SerializeField] Text textarjeta;
    [SerializeField] Text textcarpeta;
    [SerializeField] Text textobjetivo;
    private float tiempo = 1f;
    private float sigtiempo;
    void Start()
    {
        Mostrar1.enabled = true;
        Mostrar2.enabled = true;
        Mostrar3.enabled = true;
        Ocultar1.enabled = false;
        Ocultar2.enabled = false;
        Ocultar3.enabled = false;
        textcarpeta.enabled = false;
        textobjetivo.enabled = false;
    }

    public void Mostrar()
    {
        if (sigtiempo <= 0)
        {
            Debug.Log("Mostraste la Carpeta");
            Ocultar1.enabled = true;
            Ocultar2.enabled = true;
            Ocultar3.enabled = true;
            Mostrar1.enabled = false;
            Mostrar2.enabled = false;
            Mostrar3.enabled = false;
            textarjeta.enabled = false;
            textcarpeta.enabled = true;
            textobjetivo.enabled = true;
            sigtiempo = tiempo;
            FindObjectOfType<AudioManager>().Play("Sacar");
            //Aqui deberia poner el bool 'Mostrar?' del animator en true
        }
    }

    public void Ocultar()
    {
        if (sigtiempo <= 0) 
        {
            Debug.Log("Ocultaste la Carpeta");
            Mostrar1.enabled = true;
            Mostrar2.enabled = true;
            Mostrar3.enabled = true;
            Ocultar1.enabled = false;
            Ocultar2.enabled = false;
            Ocultar3.enabled = false;
            textarjeta.enabled = true;
            textcarpeta.enabled = false;
            textobjetivo.enabled = false;
            sigtiempo = tiempo;
            FindObjectOfType<AudioManager>().Play("Sacar");
            //Aqui deberia poner el bool 'Mostrar?' del animator en false
        }
    }
    void Update()
    {
        sigtiempo -= Time.deltaTime;
        if (sigtiempo < 0)
        {
            sigtiempo = 0;
        }
    }
}
