﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public void PlayGame()//Cuando se llama va a la siguiente escena
    {
        Debug.Log("Hola");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame()//Sale del juego al ser llamado
    {
        Debug.Log("Saliste");
        Application.Quit();
    }
}
